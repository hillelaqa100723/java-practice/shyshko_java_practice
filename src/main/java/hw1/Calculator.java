package hw1;

import java.util.Scanner;

public class Calculator {

    public static void main(String[] args) {
        System.out.println("Enter first value");
        Scanner myScanner = new Scanner(System.in);
        int a = myScanner.nextInt();

        System.out.println("Enter second value");
        Scanner myScanner2 = new Scanner(System.in);
        int b = myScanner2.nextInt();

        System.out.println("Your first value is " + a);
        System.out.println("Your second value is " + b);

        int rezAdd = a + b;
        System.out.println("Addition: " + rezAdd);

        int rezSub = a - b;
        System.out.println("Subtraction: " + rezSub);

        int rezMult = a * b;
        System.out.println("Multiplication: " + rezMult);

        int rezDiv = a / b;
        System.out.println("Division: " + rezDiv);

        int rezRem = a % b;
        System.out.println("Remainder: " + rezRem);
    }
}
